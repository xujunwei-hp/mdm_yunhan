-- substring_index（str,delim,count） 
-- 说明：substring_index（被截取字段，关键字，关键字出现的次数）  
--（注：如果关键字出现的次数是负数 如-2 则是从后倒数，到字符串结束） 
-- select substring_index('123mmmm,456',',',-1); -- 456
-- select substring_index('123,456',',',1); -- 123
-- select substring_index(substring_index('123,456,789',',',2),',',-1); --456
-- concat --字符串连接
select  concat('1',',',2)
-- if(判断条件，是，否)
select if(1<2,'是','否')

-- case  when
case 
   WHEN <值1> THEN <操作>
   WHEN <值2> THEN <操作>
   ...
   ELSE <操作>
END CASE;

---- 删除数字与"."
create function mdm_del_numb(orgstring varchar(4000)) returns varchar(4000)
begin 
    declare c varchar(4000);
    select replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(orgstring,'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7','')
           ,'8',''),'9',''),'.','') into c;
    return c;
end;
select mdm_del_numb("111.1mm");

----- 获取数字
drop function F_Get_num;
CREATE  FUNCTION F_Get_num (orgstring VARCHAR(4000))
RETURNS VARCHAR(4000)
begin
	 declare  c VARCHAR(1);
	 declare  i int default 0;
	 declare  str_num int;
	 declare  retstring VARCHAR(4000) default '';
     select length(orgstring)  into str_num;
      while i<=str_num do
        select  ifnull(substring(orgstring,1,1),'') into c; 
        select  ifnull(substring(orgstring,2,length(orgstring)),'') into orgstring;   
        select  concat(retstring,case when c in ('.','0','1','2','3','4','5','6','7','8','9') then c else '' end) into retstring;
        SET i = i+1;
      end while;
    RETURN retstring;
end;


--- replace 
drop table lsh_test;
create table lsh_test
(id int primary key,
a  VARCHAR(4000),
b VARCHAR(4000)
);

INSERT INTO lsh_test
	(a,b)
VALUES
	('201215121','李勇'),
	('201215122','刘晨'),
	('201215123','王敏'),
	('201215125','张立');
  
-- 示例
select * from t_dwd_spu_prop_map_cap_shixb where orginalname = 'Depth Tolerance';

update  abc t set t.propname1 ='Depth Tolerance',
  t.propval1 =case when f_del_num(proattrval)in('±mm','mm') then concat('-',replace (replace(proattrval,'±',''),'mm',''),'~',replace (replace(proattrval,'±',''),'mm',''))
                   when f_del_num(proattrval)in('+|-mm') then concat(replace (substring_index(proattrval,'|',-1),'mm',''),'~',replace (replace(substring_index(proattrval,'|',1),'+',''),'mm',''))
                   when f_del_num(proattrval)in('-mm') then concat(replace (proattrval,'mm',''))
                   when f_del_num(proattrval)='N/Rmm' then 'N/R'
                   when f_del_num(proattrval)='N/Amm' then ''
                   when proattrval ='' then ''      
                   end 
           where orginalname ='Depth Tolerance';-- 电容正负单位拆分